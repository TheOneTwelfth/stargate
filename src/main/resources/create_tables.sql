create table planet
(
	name TEXT not null,
	avgTemperature DOUBLE PRECISION,
	avgPressure DOUBLE PRECISION,
	inhabited BOOLEAN
);

create unique index planet_name_uindex
	on planet (name);


create table species
(
	planet TEXT not null,
	name TEXT not null,
	peaceful BOOLEAN
);

create unique index species_name_uindex
	on species (name);

create table state
(
  species TEXT not null,
  name TEXT not null,
  language TEXT
);

create unique index state_name_uindex
  on state (name);