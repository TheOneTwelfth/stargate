package org.bitbucket.theonetwelfth.stargate.persistence;

import org.bitbucket.theonetwelfth.stargate.domain.State;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.StateDBWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class StateDAO extends DAO<StateDBWrapper, String> {
    @Override
    public List<StateDBWrapper> getAll() {
        List<StateDBWrapper> states = new LinkedList<>();

        PreparedStatement statement = getPreparedStatement("SELECT * FROM state");
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                states.add(constructState(results));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return states;
    }

    @Override
    public StateDBWrapper getEntity(String id) {
        StateDBWrapper state = null;

        PreparedStatement statement = getPreparedStatement(String.format("SELECT * FROM state WHERE name = '%s'", id));
        try {
            ResultSet results = statement.executeQuery();
            if (results.next()) {
                state = constructState(results);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return state;
    }

    @Override
    public boolean delete(StateDBWrapper entity) {
        boolean result = false;

        PreparedStatement statement = getPreparedStatement(String.format("DELETE FROM state WHERE species = '%s' AND name = '%s'", entity.getSpeciesName(), entity.getState().getName()));
        try {
            result = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return result;
    }

    @Override
    public boolean create(StateDBWrapper entity) {
        boolean result = false;

        PreparedStatement statement = getPreparedStatement(String.format(
                "INSERT INTO state (species, name, language) " +
                        "VALUES ('%s', '%s', '%s')", entity.getSpeciesName(), entity.getState().getName(), entity.getState().getLanguage()));
        try {
            result = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return result;
    }

    private StateDBWrapper constructState(ResultSet results) throws SQLException {
        State state = State.builder()
                .setName(results.getString("name"))
                .setLanguage(results.getString("language"))
                .build();
        return new StateDBWrapper(null, state);
    }

}
