package org.bitbucket.theonetwelfth.stargate.persistence.wrapper;

import org.bitbucket.theonetwelfth.stargate.domain.State;

public class StateDBWrapper {

    private String speciesName;
    private State state;

    public StateDBWrapper(String speciesName, State state) {
        this.speciesName = speciesName;
        this.state = state;
    }

    public String getSpeciesName() { return speciesName; }
    public State getState() { return state; }

}
