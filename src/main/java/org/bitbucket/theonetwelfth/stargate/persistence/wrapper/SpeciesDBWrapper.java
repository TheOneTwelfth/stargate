package org.bitbucket.theonetwelfth.stargate.persistence.wrapper;

import org.bitbucket.theonetwelfth.stargate.domain.Species;

public class SpeciesDBWrapper {

    private String planetName;
    private Species species;

    public SpeciesDBWrapper(String planetName, Species species) {
        this.planetName = planetName;
        this.species = species;
    }

    public String getPlanetName() { return planetName; }
    public Species getSpecies() { return species; }

}
