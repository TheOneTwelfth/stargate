package org.bitbucket.theonetwelfth.stargate.persistence;

import org.bitbucket.theonetwelfth.stargate.domain.Species;
import org.bitbucket.theonetwelfth.stargate.domain.State;
import org.bitbucket.theonetwelfth.stargate.model.PopulationHandler;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.SpeciesDBWrapper;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.StateDBWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class SpeciesDAO extends DAO<SpeciesDBWrapper, String> {

    @Override
    public List<SpeciesDBWrapper> getAll() {
        List<SpeciesDBWrapper> speciesList = new LinkedList<>();

        PreparedStatement statement = getPreparedStatement("SELECT * FROM species;");
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                speciesList.add(constructSpecies(results));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return speciesList;
    }

    @Override
    public SpeciesDBWrapper getEntity(String id) {
        SpeciesDBWrapper species = null;

        PreparedStatement statement = getPreparedStatement(String.format("SELECT * FROM species WHERE name = '%s'", id));
        try {
            ResultSet results = statement.executeQuery();
            if (results.next()) {
                species = constructSpecies(results);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return species;
    }

    @Override
    public boolean delete(SpeciesDBWrapper entity) {
        boolean result = false;

        PreparedStatement speciesStatement = getPreparedStatement(String.format("DELETE FROM species WHERE name = '%s'", entity.getSpecies().getName()));
        PreparedStatement stateStatement = getPreparedStatement(String.format("DELETE FROM state WHERE species = '%s'", entity.getSpecies().getName()));
        try {
            result = speciesStatement.execute();
            result = stateStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(speciesStatement);
        }

        return result;
    }

    @Override
    public boolean create(SpeciesDBWrapper entity) {
        boolean result = false;

        PreparedStatement speciesStatement = getPreparedStatement(String.format(
                "INSERT INTO species (planet, name, peaceful) " +
                        "VALUES ('%s', '%s', %b)", entity.getPlanetName(), entity.getSpecies().getName(), entity.getSpecies().isPeaceful()));

        try {
            result = speciesStatement.execute();

            StateDAO stateDAO = new StateDAO();
            for (State state : entity.getSpecies().getStates()) {
                stateDAO.create(new StateDBWrapper(entity.getSpecies().getName(), state));
            }
            stateDAO.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(speciesStatement);
        }

        return result;
    }

    private SpeciesDBWrapper constructSpecies(ResultSet results) throws SQLException {
        SpeciesDBWrapper wrapper;

        String speciesName = results.getString("name");
        Species species = Species.builder()
                .setName(speciesName)
                .setPeaceful(results.getBoolean("peaceful"))
                .build();

        wrapper = new SpeciesDBWrapper(null, species);

        PreparedStatement statement = getPreparedStatement(String.format("SELECT name, language FROM state WHERE species = '%s'", speciesName));
        ResultSet stateResults = statement.executeQuery();
        while (stateResults.next()) {
            State state = State.builder()
                    .setName(stateResults.getString(2))
                    .setLanguage(stateResults.getString(3))
                    .build();
            species.getStates().add(state);
        }
        closePreparedStatement(statement);

        return wrapper;
    }

}
