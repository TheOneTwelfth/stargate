package org.bitbucket.theonetwelfth.stargate.persistence;

import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.domain.Species;
import org.bitbucket.theonetwelfth.stargate.domain.State;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.SpeciesDBWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class PlanetDAO extends DAO<Planet, String> {

    @Override
    public List<Planet> getAll() {
        List<Planet> planets = new LinkedList<>();

        PreparedStatement statement = getPreparedStatement(
                "SELECT planet.*, species.* FROM " +
                        "planet " +
                        "LEFT OUTER JOIN " +
                        "species " +
                            "ON planet.name = species.planet");
        try {
            ResultSet results = statement.executeQuery();
            while (results.next()) {
                planets.add(constructPlanet(results));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return planets;
    }

    @Override
    public Planet getEntity(String id) {
        Planet planet = null;

        PreparedStatement statement = getPreparedStatement(String.format(
                "SELECT planet.*, species.* FROM " +
                        "planet WHERE name = '%s' " +
                        "LEFT OUTER JOIN " +
                        "species " +
                            "ON planet.name = species.planet ", id));

        try {
            ResultSet results = statement.executeQuery();
            if (results.next()) {
                planet = constructPlanet(results);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(statement);
        }

        return planet;
    }

    @Override
    public boolean delete(Planet entity) {
        boolean result = false;

        PreparedStatement planetStatement = getPreparedStatement(String.format("DELETE FROM planet WHERE name = '%s'", entity.getName()));
        try {
            result = planetStatement.execute();
            if (entity.isInhabited()) {
                String speciesName = entity.getSpecies().getName();

                PreparedStatement speciesStatement = getPreparedStatement(String.format("DELETE FROM species WHERE name = '%s'", speciesName));
                PreparedStatement stateStatement = getPreparedStatement(String.format("DELETE FROM state WHERE species = '%s'", speciesName));

                result = speciesStatement.execute();
                result = stateStatement.execute();

                closePreparedStatement(speciesStatement);
                closePreparedStatement(stateStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(planetStatement);
        }

        return result;
    }

    @Override
    public boolean create(Planet entity) {
        boolean result = false;

        PreparedStatement planetStatement = getPreparedStatement(String.format(
                "INSERT INTO planet (name, avgTemperature, avgPressure, inhabited) " +
                        "VALUES ('%s', %f, %f, %b)", entity.getName(), entity.getAvgTemperature(), entity.getAvgPressure(), entity.isInhabited()));
        try {
            result = planetStatement.execute();
            if (entity.isInhabited() && entity.getSpecies() != null) {
                SpeciesDAO speciesDAO = new SpeciesDAO();
                speciesDAO.create(new SpeciesDBWrapper(entity.getName(), entity.getSpecies()));
                speciesDAO.closeConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(planetStatement);
        }

        return result;
    }

    private Planet constructPlanet(ResultSet results) throws SQLException {
        Planet planet = Planet.builder()
                .setName(results.getString(1))
                .setAvgTemperature(results.getDouble(2))
                .setAvgPressure(results.getDouble(3))
                .setInhabited(results.getBoolean(4))
                .build();


        if (planet.isInhabited()) {
            String speciesName = results.getString(6);
            Species species = Species.builder()
                    .setName(speciesName)
                    .setPeaceful(results.getBoolean(7))
                    .build();
            planet.setSpecies(species);
            PreparedStatement statement = getPreparedStatement(String.format("SELECT name, language FROM state WHERE species = '%s'", speciesName));
            ResultSet stateResults = statement.executeQuery();
            while (stateResults.next()) {
                State state = State.builder()
                        .setName(stateResults.getString("name"))
                        .setLanguage(stateResults.getString("language"))
                        .build();
                species.getStates().add(state);
            }
            closePreparedStatement(statement);
        }
        return planet;
    }

}
