package org.bitbucket.theonetwelfth.stargate.persistence;

import org.postgresql.jdbc2.optional.PoolingDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public final class ConnectionPool {

    private static PoolingDataSource connectionPool = null;

    private static final String DB_NAME = "stargate";
    private static final String DB_USER = "oop";
    private static final String DB_PASS = "123";

    private ConnectionPool() {}

    public static void init() {
        if (connectionPool == null) {
            connectionPool = new PoolingDataSource();
            connectionPool.setDataSourceName("Postgres");
            connectionPool.setServerName("localhost");
            connectionPool.setDatabaseName(DB_NAME);
            connectionPool.setUser(DB_USER);
            connectionPool.setPassword(DB_PASS);
            connectionPool.setMaxConnections(10);
        }
    }

    static Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }

}
