package org.bitbucket.theonetwelfth.stargate;

public class EntryPoint {

    public static void main(String[] args) {
        StargateApp app = StargateApp.INSTANCE;
        app.start();
    }

}
