package org.bitbucket.theonetwelfth.stargate;

import org.bitbucket.theonetwelfth.stargate.persistence.ConnectionPool;
import org.bitbucket.theonetwelfth.stargate.model.Catalogue;
import org.bitbucket.theonetwelfth.stargate.ui.cli.CLI;
import org.bitbucket.theonetwelfth.stargate.ui.gui.GUI;
import org.bitbucket.theonetwelfth.stargate.ui.Interactable;

public class StargateApp {

    public static final StargateApp INSTANCE = new StargateApp();

    private Interactable ui;
    private Catalogue catalogue;

    void start() {
        ConnectionPool.init();
        ui = new GUI();
        catalogue = new Catalogue();
        ui.start();
    }

    private StargateApp() {}

    public Interactable getUI() { return ui; }
    public Catalogue getCatalogue() { return catalogue; }

    public void setUI(Interactable ui) {
        this.ui = ui;
    }

}
