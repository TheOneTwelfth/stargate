package org.bitbucket.theonetwelfth.stargate.model;

import org.bitbucket.theonetwelfth.stargate.StargateApp;
import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.domain.Species;
import org.bitbucket.theonetwelfth.stargate.persistence.PlanetDAO;

import java.util.*;

public class Catalogue {

    private Map<String, Planet> planets;
    private Planet currentPlanet;

    private List<CatalogueListener> listeners;

    public Catalogue() {
        planets = new HashMap<>();
        listeners = new LinkedList<>();

        PlanetDAO planetDAO = new PlanetDAO();
        List<Planet> planetsInDB = planetDAO.getAll();
        for (Planet planet : planetsInDB) {
            planets.put(planet.getName(), planet);
        }
        planetDAO.closeConnection();

        currentPlanet = null;
    }

    public void addPlanet(String[] args) {
        String name = args[0];
        double avgTemperature;
        double avgPressure;
        boolean inhabited;

        try {
            avgTemperature = Double.parseDouble(args[1]);
        } catch (NumberFormatException e) { avgTemperature = 0.0; }
        try {
            avgPressure = Double.parseDouble(args[2]);
        } catch (NumberFormatException e) { avgPressure = 0.0; }
        try {
            inhabited = Boolean.parseBoolean(args[3]);
        } catch (NumberFormatException e) { inhabited = false; }

        Planet planet = Planet.builder()
                .setName(name)
                .setAvgTemperature(avgTemperature)
                .setAvgPressure(avgPressure)
                .setInhabited(inhabited)
                .build();

        addPlanet(planet);

    }

    private void addPlanet(Planet planet) {
        planets.put(planet.getName(), planet);

        PlanetDAO planetDAO = new PlanetDAO();
        planetDAO.create(planet);
        planetDAO.closeConnection();

        for (CatalogueListener listener : listeners)
            listener.planetAdded(planet.getName());
    }

    public boolean removePlanet(String planetName) {
        boolean planetExists = planets.containsKey(planetName);
        if (planetExists) {
            PlanetDAO planetDAO = new PlanetDAO();
            planetDAO.delete(planets.get(planetName));
            planetDAO.closeConnection();

            for (CatalogueListener listener : listeners)
                listener.planetRemoved(planetName);

        }
        planets.remove(planetName);
        return planetExists;
    }

    public boolean addPlanetState(String planetName, String stateName, String stateLanguage) {
        if (!planetExists(planetName))
            return false;

        Planet planet = planets.get(planetName);
        if (!planet.isInhabited())
            return false;

        Species species = planet.getSpecies();
        PopulationHandler.updateState(species, stateName, stateLanguage);

        return true;
    }

    public boolean addPlanetSpecies(String planetName, String speciesName, boolean speciesPeaceful) {
        if (!planetExists(planetName))
            return false;

        Planet planet = planets.get(planetName);

        PopulationHandler.updateSpecies(planet, speciesName, speciesPeaceful);

        return true;
    }

    public String getCurrentPlanetName() {
        return currentPlanet == null ? "BASE" : currentPlanet.getName();
    }

    public String getCurrentPlanet() {

        if (currentPlanet == null)
            return "Current location: \n Base";

        return "Current location: \n" + currentPlanet.toString();

    }

    public void travelToPlanet(String planetName) {
        if (planetExists(planetName)) {
            WarpHandler.performWarp(planetName);
        }
    }

    void arriveAtPlanet(String planetName) {
        if (planetExists(planetName)) {
            currentPlanet = planets.get(planetName);
            for (CatalogueListener listener : listeners)
                listener.planetChanged(getCurrentPlanetName());
        }
        else {
            StargateApp.INSTANCE.getUI().showMessage("Something went wrong, returning to base!");
            leavePlanet();
        }
    }

    public boolean leavePlanet() {
        if (currentPlanet != null) {
            currentPlanet = null;
            for (CatalogueListener listener : listeners)
                listener.planetChanged(getCurrentPlanetName());
            return true;
        }
        else
            return false;
    }

    public boolean planetInhabited(String planetName) {
        if (!planetExists(planetName))
            return false;
        Planet planet = planets.get(planetName);
        return planet.isInhabited();
    }

    public boolean planetSpeciesPeaceful(String planetName) {
        if (!planetExists(planetName))
            return false;
        Planet planet = planets.get(planetName);

        if (!planet.isInhabited())
            return false;

        return planet.getSpecies().isPeaceful();
    }

    public Planet getPlanet(String planetName) {
        return planets.get(planetName);
    }

    public boolean planetExists(String name) { return planets.containsKey(name); }

    public Set<String> getPlanetNames() {
        return planets.keySet();
    }

    public void registerListener(CatalogueListener listener) {
        listeners.add(listener);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Planet planet : planets.values()) {
            builder.append(planet.toString());
            builder.append("\n");
        }

        return builder.toString();
    }

}
