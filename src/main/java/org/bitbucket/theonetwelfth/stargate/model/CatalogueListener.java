package org.bitbucket.theonetwelfth.stargate.model;

public interface CatalogueListener {

    void planetAdded(String name);
    void planetRemoved(String name);

    void planetChanged(String name);

}
