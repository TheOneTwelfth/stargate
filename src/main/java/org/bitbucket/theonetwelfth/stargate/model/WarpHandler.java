package org.bitbucket.theonetwelfth.stargate.model;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class WarpHandler {

    public static boolean warpInProgress = false;

    static void performWarp(String planetName) {
        WarpProgressObserver observer = new WarpProgressObserver(planetName);
        WarpProgressWorker worker = new WarpProgressWorker(observer);
        Thread warpThread = new Thread(worker);
        warpThread.start();
    }

}

class WarpProgressObserver implements Observer {

    private String planetName;

    WarpProgressObserver(String planetName) {
        this.planetName = planetName;
    }

    @Override
    public void stateChanged(int state) {
        StargateApp.INSTANCE.getUI().showProgressBar(state);
        if (state == 10) {
            StargateApp.INSTANCE.getUI().showMessage("Warp complete!");
            StargateApp.INSTANCE.getCatalogue().arriveAtPlanet(planetName);
            WarpHandler.warpInProgress = false;
        }
    }
}

class WarpProgressWorker implements Runnable {

    private int counter;
    private Observer observer;

    WarpProgressWorker(Observer observer) {
        counter = 0;
        this.observer = observer;
    }

    @Override
    public void run() {
        WarpHandler.warpInProgress = true;
        try {
            while (counter <= 10) {
                observer.stateChanged(counter);
                Thread.sleep(1000);
                counter++;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            WarpHandler.warpInProgress = false;
        }
    }

}