package org.bitbucket.theonetwelfth.stargate.model;

public interface Observer {

    public void stateChanged(int state);

}
