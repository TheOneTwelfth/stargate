package org.bitbucket.theonetwelfth.stargate.model;

import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.domain.Species;
import org.bitbucket.theonetwelfth.stargate.domain.State;
import org.bitbucket.theonetwelfth.stargate.persistence.SpeciesDAO;
import org.bitbucket.theonetwelfth.stargate.persistence.StateDAO;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.SpeciesDBWrapper;
import org.bitbucket.theonetwelfth.stargate.persistence.wrapper.StateDBWrapper;

public final class PopulationHandler {

    private PopulationHandler() {}

    public static void updateSpecies(Planet planet, String speciesName, boolean isPeaceful) {
        Species species = Species.builder()
                .setName(speciesName)
                .setPeaceful(isPeaceful)
                .build();

        SpeciesDAO speciesDAO = new SpeciesDAO();

        if (planet.getSpecies() != null) {
            speciesDAO.delete(new SpeciesDBWrapper(planet.getName(), planet.getSpecies()));
        }
        speciesDAO.create(new SpeciesDBWrapper(planet.getName(), species));

        planet.setSpecies(species);
    }

    public static void updateState(Species species, String stateName, String stateLanguage) {
        State state = State.builder()
                .setName(stateName)
                .setLanguage(stateLanguage)
                .build();
        species.getStates().add(state);

        StateDAO stateDAO = new StateDAO();
        stateDAO.create(new StateDBWrapper(species.getName(), state));
    }

}
