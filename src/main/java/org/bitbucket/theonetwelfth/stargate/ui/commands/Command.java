package org.bitbucket.theonetwelfth.stargate.ui.commands;

public abstract class Command {

    String[] args;

    public abstract String execute();

    public void setArgs(String[] args) {
        this.args = args;
    }

}
