package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class ListPlanetsCommand extends Command {

    @Override
    public String execute() {
        return "Planets in catalogue: " +
                "\n" +
                StargateApp.INSTANCE.getCatalogue().toString();
    }

    public static String getDescription() { return "lists planets in the catalogue"; }

}
