package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.bitbucket.theonetwelfth.stargate.StargateApp;
import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.ui.CommandParser;
import org.bitbucket.theonetwelfth.stargate.ui.Interactable;

public class GUI extends Application implements Interactable {

    private GUIController controller;

    @Override
    public void start() {
        launch();
    }

    @Override
    public void run() {}

    @Override
    public CommandParser getCommandParser() {
        return null;
    }

    @Override
    public void showMessage(String message) {
        Platform.runLater(() -> controller.showAlert(message));
    }

    @Override
    public void showProgressBar(int count) {
        controller.modifyProgressBar(0.1*count);
    }

    @Override
    public void showPlanetInfo(Planet planet) {
        controller.showPlanetInfoDialog(planet);
    }

    @Override
    public boolean askBooleanDialogue(String prompt) {
        return controller.showBooleanDialog(prompt);
    }

    @Override
    public boolean askConfirmation(String prompt) {
        return controller.showConfirmation(prompt);
    }

    @Override
    public String askStringDialogue(String prompt) {
        return controller.showTextInput(prompt);
    }

    @Override
    public void kill() {
        Platform.exit();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui.fxml"));

        Parent root = loader.load();
        controller = loader.getController();

        StargateApp.INSTANCE.setUI(this);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        primaryStage.setTitle("StarGate");
        primaryStage.setResizable(false);
        primaryStage.show();

    }

}
