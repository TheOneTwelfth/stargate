package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class HelpCommand extends Command {

    @Override
    public String execute() {
        HashMap<String, Class<? extends Command>> commands =
                StargateApp.INSTANCE.getUI().getCommandParser().getCommands();

        String[] commandNames = new String[commands.keySet().size()];
        Iterator<String> it = commands.keySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            commandNames[i] = it.next();
            i++;
        }
        Arrays.sort(commandNames);

        StringBuilder builder = new StringBuilder();
        for (String commandName : commandNames) {
            try {
                Class<? extends Command> command = commands.get(commandName);
                builder.append(commandName);
                try {
                    builder.append(" ");
                    builder.append(command.getMethod("getUsage").invoke(null));
                } catch (NoSuchMethodException ignored) {}
                try {
                    builder.append(" - ");
                    builder.append(command.getMethod("getDescription").invoke(null));
                } catch (NoSuchMethodException ignored) {}
                builder.append("\n");
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return builder.toString();
    }

    @Override
    public void setArgs(String[] args) {}

    public static String getDescription() { return "prints help prompt"; }

}
