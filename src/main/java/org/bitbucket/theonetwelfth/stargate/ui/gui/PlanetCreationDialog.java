package org.bitbucket.theonetwelfth.stargate.ui.gui;


import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

class PlanetCreationDialog extends Dialog<String[]> {


    PlanetCreationDialog() {
        super();

        setTitle("Add new planet");
        setHeaderText("Fill in the planet details");

        ButtonType createButtonType = new ButtonType("Create", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10.0);
        grid.setVgap(10.0);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setPromptText("Name");
        TextField avgTemperature = new TextField();
        avgTemperature.setPromptText("Average Temperature");
        TextField avgPressure = new TextField();
        avgPressure.setPromptText("Average Pressure");
        CheckBox inhabited = new CheckBox();

        grid.add(new Label("Name: "), 0, 0);
        grid.add(new Label("Average Temperature: "), 0, 1);
        grid.add(new Label("Average Pressure: "), 0, 2);
        grid.add(new Label("Is Inhabited: "), 0, 3);

        grid.add(name, 1, 0);
        grid.add(avgTemperature, 1, 1);
        grid.add(avgPressure, 1, 2);
        grid.add(inhabited, 1, 3);

        Node createButton = getDialogPane().lookupButton(createButtonType);
        createButton.setDisable(true);

        name.textProperty().addListener(((observable, oldValue, newValue) -> createButton.setDisable(newValue.trim().isEmpty())));

        getDialogPane().setContent(grid);

        Platform.runLater(name::requestFocus);

        setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                String[] result = new String[4];
                result[0] = name.getText();
                result[1] = avgTemperature.getText();
                result[2] = avgPressure.getText();
                result[3] = String.valueOf(inhabited.isSelected());
                return result;
            }
            return null;
        });

    }

}
