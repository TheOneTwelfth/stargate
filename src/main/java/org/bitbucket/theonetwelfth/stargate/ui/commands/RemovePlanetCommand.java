package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class RemovePlanetCommand extends Command {

    @Override
    public String execute() {
        if (StargateApp.INSTANCE.getCatalogue().getCurrentPlanetName().equals(args[0]))
            return "Can't remove current planet!";
        boolean success = StargateApp.INSTANCE.getCatalogue().removePlanet(args[0]);
        if (success)
            return "Planet " + args[0] + " removed successfully!";
        else
            return "Planet " + args[0] + " does not exist";
    }

    public static String getUsage() { return "<name>"; }
    public static String getDescription() { return "removes a planet from the catalogue"; }

}
