package org.bitbucket.theonetwelfth.stargate.ui.cli;

import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.ui.CommandParser;
import org.bitbucket.theonetwelfth.stargate.ui.Interactable;

public class CLI implements Interactable {

    private static final String DEFAULT_PROMPT = "(StargateConsole) > ";

    private InputHandler inputHandler;
    private OutputHandler outputHandler;

    private CommandParser commandParser;

    private boolean timeToDie;

    @Override
    public void start() {
        inputHandler = new InputHandler(System.in);
        outputHandler = new OutputHandler(System.out);

        commandParser = new CommandParser();

        timeToDie = false;

        run();
    }

    @Override
    public void run() {
        while (!timeToDie) {
            outputHandler.print(DEFAULT_PROMPT);
            String rawCommand = inputHandler.awaitInput();
            String commandResult = commandParser.parseAndExecute(rawCommand);
            outputHandler.println(commandResult);
        }
    }

    public CommandParser getCommandParser() { return commandParser; }

    @Override
    public void showMessage(String message) {
        outputHandler.print(message);
    }

    @Override
    public void showProgressBar(int count) {
        outputHandler.print("[");
        for (int i=0;i<10;i++) {
            if (i < count)
                outputHandler.print("█");
            else
                outputHandler.print(" ");
        }
        outputHandler.print("]\n");
    }

    @Override
    public void showPlanetInfo(Planet planet) {
        outputHandler.println(planet.toString());
    }

    @Override
    public boolean askBooleanDialogue(String prompt) {
        String response = askStringDialogue(prompt);
        boolean result;
        try {
            result = Boolean.parseBoolean(response);
        } catch (NumberFormatException e) { result = false; }
        return result;
    }

    @Override
    public boolean askConfirmation(String prompt) {
        return askBooleanDialogue(prompt);
    }

    @Override
    public String askStringDialogue(String prompt) {
        outputHandler.print(prompt);
        String response;
        do {
            response = inputHandler.awaitInput();
        } while (response == null);
        return response;
    }

    @Override
    public void kill() { timeToDie = true; }

}
