package org.bitbucket.theonetwelfth.stargate.ui.cli;

import java.io.InputStream;
import java.util.Scanner;

class InputHandler {

    private Scanner scanner;

    InputHandler(InputStream stream) {
        scanner = new Scanner(stream);
    }

    String awaitInput() {
        return scanner.nextLine();
    }

}
