package org.bitbucket.theonetwelfth.stargate.ui;

import org.bitbucket.theonetwelfth.stargate.domain.Planet;

public interface Interactable {

    void start();

    void run();

    CommandParser getCommandParser();

    void showMessage(String message);
    void showProgressBar(int count);

    void showPlanetInfo(Planet planet);

    boolean askBooleanDialogue(String prompt);
    boolean askConfirmation(String prompt);
    String askStringDialogue(String prompt);

    void kill();

}
