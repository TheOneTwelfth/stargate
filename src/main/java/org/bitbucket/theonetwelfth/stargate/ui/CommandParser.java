package org.bitbucket.theonetwelfth.stargate.ui;

import org.bitbucket.theonetwelfth.stargate.ui.commands.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;

public class CommandParser {

    private HashMap<String, Class<? extends Command>> registeredCommands;

    public CommandParser() {
        registeredCommands = new HashMap<>();

        registeredCommands.put("help", HelpCommand.class);
        registeredCommands.put("exit", ExitCommand.class);
        registeredCommands.put("addplanet", AddPlanetCommand.class);
        registeredCommands.put("removeplanet", RemovePlanetCommand.class);
        registeredCommands.put("listplanets", ListPlanetsCommand.class);
        registeredCommands.put("planetinfo", PlanetInfoCommand.class);
        registeredCommands.put("addstate", AddStateCommand.class);
        registeredCommands.put("warp", WarpCommand.class);
        registeredCommands.put("whereami", WhereAmICommand.class);
    }

    public String parseAndExecute(String rawCommand) {
        String[] rawCommandSplit = rawCommand.split(" ");
        String commandString = rawCommandSplit[0];
        if (registeredCommands.containsKey(commandString)) {
            Class<? extends Command> commandClass = registeredCommands.get(commandString);
            String commandResult = null;
            try {
                Command command = commandClass.getConstructor().newInstance();
                if (rawCommandSplit.length > 1)
                    command.setArgs(Arrays.copyOfRange(rawCommandSplit, 1, rawCommandSplit.length));
                commandResult = command.execute();
            } catch (NoSuchMethodException |
                    IllegalAccessException |
                    InvocationTargetException |
                    InstantiationException e) {
                e.printStackTrace();
            }

            return commandResult;
        }
        else
            return "Command not found!";
    }

    public String execute(Command command, String[] args) {
        command.setArgs(args);
        return command.execute();
    }

    public HashMap<String, Class<? extends Command>> getCommands() { return registeredCommands; }
}
