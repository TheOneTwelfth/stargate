package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;
import org.bitbucket.theonetwelfth.stargate.model.Catalogue;

public class PlanetInfoCommand extends Command {

    @Override
    public String execute() {
        Catalogue catalogue = StargateApp.INSTANCE.getCatalogue();
        if (catalogue.planetExists(args[0])) {
            StargateApp.INSTANCE.getUI().showPlanetInfo(catalogue.getPlanet(args[0]));
            return null;
        }
        else
            return "Planet does not exist!";
    }

}
