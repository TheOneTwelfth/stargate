package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.bitbucket.theonetwelfth.stargate.StargateApp;
import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.ui.CommandParser;
import org.bitbucket.theonetwelfth.stargate.ui.commands.AddPlanetCommand;
import org.bitbucket.theonetwelfth.stargate.ui.commands.ExitCommand;

import java.util.Optional;

public class GUIController {

    private CommandParser commandParser;

    @FXML
    private ListView<String> planetList;

    @FXML
    private Button exitBtn;
    @FXML
    private Button addPlanetBtn;

    @FXML
    private ProgressBar warpProgressBar;

    @FXML
    private Label currentPlanetLabel;

    void modifyProgressBar(double value) {
        warpProgressBar.setProgress(value);
    }

    void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
        alert.setHeaderText(null);
        alert.show();
    }

    void showPlanetInfoDialog(Planet planet) {
        PlanetInfoDialog dialog = new PlanetInfoDialog(planet, commandParser);
        dialog.show();
    }

    boolean showConfirmation(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message);
        alert.setHeaderText(null);
        alert.setTitle("Warning!");
        alert.showAndWait();

        return alert.getResult() == ButtonType.OK;
    }

    boolean showBooleanDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message);
        alert.setHeaderText(null);
        alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);

        alert.showAndWait();

        return alert.getResult() == ButtonType.YES;
    }

    String showTextInput(String message) {
        Optional<String> result;
        do {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setContentText(message);
            result = dialog.showAndWait();
        } while (!result.isPresent());
        return result.get();
    }

    @FXML
    public void initialize() {

        commandParser = new CommandParser();

        planetList.setCellFactory(param -> new PlanetCell(commandParser));

        for (String planetName : StargateApp.INSTANCE.getCatalogue().getPlanetNames())
            planetList.getItems().add(planetName);

        StargateApp.INSTANCE.getCatalogue().registerListener(new PlanetListListener(planetList, currentPlanetLabel));

        addPlanetBtn.setOnAction(action -> {
            PlanetCreationDialog dialog = new PlanetCreationDialog();
            Optional<String[]> result = dialog.showAndWait();

            result.ifPresent(args -> commandParser.execute(new AddPlanetCommand(), args));

        });

        exitBtn.setOnAction(action -> commandParser.execute(new ExitCommand(), null));

    }

}
