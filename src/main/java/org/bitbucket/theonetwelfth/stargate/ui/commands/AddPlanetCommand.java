package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.ui.Interactable;
import org.bitbucket.theonetwelfth.stargate.model.Catalogue;
import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class AddPlanetCommand extends Command {

    @Override
    public String execute() {
        Catalogue catalogue = StargateApp.INSTANCE.getCatalogue();
        String name = args[0];
        if (!catalogue.planetExists(name)) {

            catalogue.addPlanet(args);

            if (catalogue.planetInhabited(name)) {
                Interactable ui = StargateApp.INSTANCE.getUI();

                String speciesName = ui.askStringDialogue("Please specify species name: ");
                boolean speciesPeaceful = ui.askBooleanDialogue("Please specify if the species is peaceful: ");

                catalogue.addPlanetSpecies(name, speciesName, speciesPeaceful);
                ui.showMessage("Species " + speciesName + " added successfully!\n");
            }

            return "Planet " + name + " added successfully!";
        }
        else
            return "Planet already exists!";
    }

    @Override
    public void setArgs(String[] args) {
        this.args = args;
    }

    public static String getUsage() { return "<name> <average temperature> <average pressure> <is inhabited>"; }
    public static String getDescription() { return "adds a new planet to the catalogue"; }

}
