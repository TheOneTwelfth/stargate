package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.model.WarpHandler;
import org.bitbucket.theonetwelfth.stargate.ui.Interactable;
import org.bitbucket.theonetwelfth.stargate.model.Catalogue;
import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class WarpCommand extends Command {

    @Override
    public String execute() {
        Catalogue catalogue = StargateApp.INSTANCE.getCatalogue();

        if (args[0].equals("BASE")) {
            boolean success = catalogue.leavePlanet();
            if (success)
                return "Successfully warped to base!";
            else
                return "Already at the base.";
        }

        if (catalogue.planetExists(args[0])) {
            if (catalogue.getCurrentPlanetName().equals(args[0]))
                return "Already at planet " + args[0] + "!";
            if (!catalogue.planetInhabited(args[0]) || catalogue.planetSpeciesPeaceful(args[0])) {
                if (!WarpHandler.warpInProgress) {
                    catalogue.travelToPlanet(args[0]);
                    return "Warp to " + args[0] + " in progress...";
                }
                else
                    return "Already warping!";
            }
            else {
                Interactable ui = StargateApp.INSTANCE.getUI();

                boolean confirmed = ui.askConfirmation("WARNING! The planet contains hostile inhabitants. Do you wish to proceed? ");

                if (confirmed) {
                    catalogue.travelToPlanet(args[0]);
                    return "Warp to " + args[0] + " in progress...";
                }
                else
                    return "Warp cancelled.";
            }
        }
        else
            return "The specified planet does not exist!";
    }

    public static String getUsage() { return "<planet name/BASE>"; }
    public static String getDescription() { return "warps to the specified planet"; }

}
