package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import org.bitbucket.theonetwelfth.stargate.StargateApp;
import org.bitbucket.theonetwelfth.stargate.ui.CommandParser;
import org.bitbucket.theonetwelfth.stargate.ui.commands.PlanetInfoCommand;
import org.bitbucket.theonetwelfth.stargate.ui.commands.RemovePlanetCommand;
import org.bitbucket.theonetwelfth.stargate.ui.commands.WarpCommand;

class PlanetCell extends ListCell<String> {
    private HBox hBox = new HBox();
    private Label label = new Label("");
    private String lastItem;

    PlanetCell(CommandParser commandParser) {
        super();

        Button infoButton = new Button("Info");
        Button warpButton = new Button("Warp");
        Button removeButton = new Button("Remove");

        label.setFont(new Font(14.0));

        Pane pane = new Pane();
        hBox.getChildren().addAll(label, pane, infoButton, warpButton, removeButton);
        hBox.setSpacing(2.0);
        HBox.setHgrow(pane, Priority.ALWAYS);

        infoButton.setOnAction(action -> {
            String[] args = new String[1];
            args[0] = lastItem;
            commandParser.execute(new PlanetInfoCommand(), args);
        });

        warpButton.setOnAction(action -> {
            String[] args = new String[1];
            args[0] = lastItem;
            String result = commandParser.execute(new WarpCommand(), args);
            StargateApp.INSTANCE.getUI().showMessage(result);
        });

        removeButton.setOnAction(action -> {
            String[] args = new String[1];
            args[0] = lastItem;
            String result = commandParser.execute(new RemovePlanetCommand(), args);
            StargateApp.INSTANCE.getUI().showMessage(result);
        });

    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if (empty) {
            lastItem = null;
            setGraphic(null);
        }
        else {
            lastItem = item;
            label.setText(item != null ? item : "null");
            setGraphic(hBox);
        }
    }

}
