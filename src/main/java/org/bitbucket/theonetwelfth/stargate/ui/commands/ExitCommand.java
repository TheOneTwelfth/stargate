package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class ExitCommand extends Command {
    @Override
    public String execute() {
        StargateApp.INSTANCE.getUI().kill();
        return null;
    }

    @Override
    public void setArgs(String[] args) {}

    public static String getDescription() { return "exits the application"; }

}
