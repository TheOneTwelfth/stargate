package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class WhereAmICommand extends Command {

    @Override
    public String execute() {
        return StargateApp.INSTANCE.getCatalogue().getCurrentPlanet();
    }

    public static String getDescription() { return "gets the current location"; }

}
