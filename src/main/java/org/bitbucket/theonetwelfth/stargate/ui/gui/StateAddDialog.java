package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

class StateAddDialog extends Dialog<String[]> {

    StateAddDialog() {
        super();

        setTitle("Add new state");
        setHeaderText("Fill in state details");

        ButtonType createButtonType = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10.0);
        grid.setVgap(10.0);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setPromptText("Name");
        TextField language = new TextField();
        language.setPromptText("Language");

        grid.add(new Label("Name: "), 0, 0);
        grid.add(new Label("Language: "), 0, 1);

        grid.add(name, 1, 0);
        grid.add(language, 1, 1);

        Node createButton = getDialogPane().lookupButton(createButtonType);
        createButton.setDisable(true);

        name.textProperty().addListener(((observable, oldValue, newValue) -> createButton.setDisable(newValue.trim().isEmpty())));

        getDialogPane().setContent(grid);

        Platform.runLater(name::requestFocus);

        setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                String[] result = new String[3];
                result[1] = name.getText();
                result[2] = language.getText();
                return result;
            }
            return null;
        });

    }

}
