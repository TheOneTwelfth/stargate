package org.bitbucket.theonetwelfth.stargate.ui.commands;

import org.bitbucket.theonetwelfth.stargate.StargateApp;

public class AddStateCommand extends Command {

    @Override
    public String execute() {
        boolean success = StargateApp.INSTANCE.getCatalogue().addPlanetState(args[0], args[1], args[2]);
        if (success)
            return "State " + args[1] + " added to planet " + args[0] + " successfully!";
        else
            return "Planet " + args[0] + " does not exist or is uninhabited.";
    }

    public static String getUsage() { return "<planet name> <state name> <state language>"; }
    public static String getDescription() { return "add state to the species living on a planet"; }

}
