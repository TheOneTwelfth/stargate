package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import org.bitbucket.theonetwelfth.stargate.domain.Planet;
import org.bitbucket.theonetwelfth.stargate.domain.State;
import org.bitbucket.theonetwelfth.stargate.ui.CommandParser;
import org.bitbucket.theonetwelfth.stargate.ui.commands.AddStateCommand;

import javax.lang.model.type.NullType;
import java.util.Optional;

class PlanetInfoDialog extends Dialog<NullType> {

    private ListView<String> stateList;
    private Planet planet;

    PlanetInfoDialog(Planet planet, CommandParser commandParser) {
        super();

        this.planet = planet;

        setTitle("Planet " + planet.getName());
        setHeaderText("Details on planet " + planet.getName());

        getDialogPane().getButtonTypes().setAll(ButtonType.OK);

        GridPane grid = new GridPane();
        grid.setHgap(10.0);
        grid.setVgap(10.0);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField avgTemperature = new TextField(String.valueOf(planet.getAvgTemperature()));
        avgTemperature.setEditable(false);
        TextField avgPressure = new TextField(String.valueOf(planet.getAvgPressure()));
        avgPressure.setEditable(false);
        CheckBox inhabited = new CheckBox() {
            @Override
            public void arm() {}
        };
        inhabited.setSelected(planet.isInhabited());

        grid.add(new Label("Average Temperature: "), 0, 0);
        grid.add(new Label("Average Pressure: "), 0, 1);
        grid.add(new Label("Is Inhabited: "), 0, 2);

        grid.add(avgTemperature, 1, 0);
        grid.add(avgPressure, 1, 1);
        grid.add(inhabited, 1, 2);

        if (planet.isInhabited()) {
            TextField species = new TextField(planet.getSpecies().getName());
            species.setEditable(false);

            CheckBox peaceful = new CheckBox() {
                @Override
                public void arm() {}
            };
            peaceful.setSelected(planet.getSpecies().isPeaceful());

            stateList = new ListView<>();
            updateStateList();

            Button addStateBtn = new Button("Add State");
            addStateBtn.setOnAction(action -> {
                StateAddDialog dialog = new StateAddDialog();
                Optional<String[]> result = dialog.showAndWait();
                result.ifPresent(args -> {
                    args[0] = planet.getName();
                    commandParser.execute(new AddStateCommand(), args);
                    updateStateList();
                });
            });

            grid.add(new Label("Species: "), 0, 3);
            grid.add(new Label("Is Peaceful: "), 0, 4);
            grid.add(new Label("States: "), 0, 5);

            grid.add(species, 1, 3);
            grid.add(peaceful, 1, 4);
            grid.add(stateList, 1, 5);
            grid.add(addStateBtn, 1, 6);

        }

        getDialogPane().setContent(grid);

        setResultConverter(dialogButton -> null);
    }

    private void updateStateList() {
        stateList.getItems().clear();
        for (State state : planet.getSpecies().getStates()) {
            stateList.getItems().add(state.getName() + " speaking " + state.getLanguage());
        }
    }

}
