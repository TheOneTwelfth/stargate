package org.bitbucket.theonetwelfth.stargate.ui.cli;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

class OutputHandler {

    private BufferedWriter writer;

    OutputHandler(OutputStream stream) {
        this.writer = new BufferedWriter(new OutputStreamWriter(stream));
    }

    void print(String output) {
        try {
            writer.write(output);
            writer.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    void println(String output) {
        if (output != null)
            print(output + "\n");
    }

}
