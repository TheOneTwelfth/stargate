package org.bitbucket.theonetwelfth.stargate.ui.gui;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import org.bitbucket.theonetwelfth.stargate.model.CatalogueListener;

public class PlanetListListener implements CatalogueListener {

    private ListView<String> list;
    private Label currentPlanet;

    PlanetListListener(ListView<String> list, Label currentPlanet) {
        this.list = list;
        this.currentPlanet = currentPlanet;
    }

    @Override
    public void planetAdded(String name) {
        list.getItems().add(name);
    }

    @Override
    public void planetRemoved(String name) {
        list.getItems().remove(name);
    }

    @Override
    public void planetChanged(String name) {
        Platform.runLater(() -> currentPlanet.setText(name));
    }

}
