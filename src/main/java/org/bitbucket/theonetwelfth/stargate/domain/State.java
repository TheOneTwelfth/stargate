package org.bitbucket.theonetwelfth.stargate.domain;

import java.io.Serializable;

public class State implements Serializable {

    private String name;
    private String language;

    private State() {}

    public String getName() { return name; }
    public String getLanguage() { return language; }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        State other = (State) o;

        return this.name.equals(other.name);
    }

    @Override
    public String toString() {
        return String.format("%s speaking %s", name, language);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public static Builder builder() { return new State().new Builder(); }

    public class Builder {

        private Builder() {}

        public Builder setName(String name) {
            State.this.name = name;
            return this;
        }
        public Builder setLanguage(String language) {
            State.this.language = language;
            return this;
        }

        public State build() { return State.this; }

    }

}
