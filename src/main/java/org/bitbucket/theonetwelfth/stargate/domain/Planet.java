package org.bitbucket.theonetwelfth.stargate.domain;

import java.io.Serializable;

public class Planet implements Serializable {

    private String name;
    private double avgTemperature;
    private double avgPressure;
    private boolean inhabited;

    private Species species;

    private Planet() {}

    public String getName() { return name; }
    public double getAvgTemperature() { return avgTemperature; }
    public double getAvgPressure() { return avgPressure; }
    public boolean isInhabited() { return inhabited; }
    public Species getSpecies() { return species; }

    public void setSpecies(Species species) { this.species = species; }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Planet other = (Planet) o;

        return this.name.equals(other.name);
    }

    @Override
    public String toString() {
        StringBuilder dump = new StringBuilder();

        dump.append("Planet ").append(name).append(":\n");
        dump.append("- Average temperature: ").append(avgTemperature).append("K\n");
        dump.append("- Average pressure: ").append(avgPressure).append("KPa\n");
        dump.append("- The planet is ");
        if (inhabited) {
            dump.append("inhabited by ");
            dump.append(species);
        }
        else
            dump.append("not inhabited\n");

        return dump.toString();
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public static Builder builder() { return new Planet().new Builder(); }

    public class Builder {

        private Builder() {}

        public Builder setName(String name) {
            Planet.this.name = name;
            return this;
        }
        public Builder setAvgTemperature(double avgTemperature) {
            Planet.this.avgTemperature = avgTemperature;
            return this;
        }
        public Builder setAvgPressure(double avgPressure) {
            Planet.this.avgPressure = avgPressure;
            return this;
        }
        public Builder setInhabited(boolean inhabited) {
            Planet.this.inhabited = inhabited;
            return this;
        }

        public Planet build() { return Planet.this; }

    }

}
