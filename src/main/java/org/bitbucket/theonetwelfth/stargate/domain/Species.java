package org.bitbucket.theonetwelfth.stargate.domain;

import java.io.Serializable;
import java.util.HashSet;

public class Species implements Serializable {

    private String name;
    private HashSet<State> states;
    private boolean peaceful;

    private Species() {
        this.states = new HashSet<>();
    }

    public String getName() { return name; }
    public HashSet<State> getStates() { return states; }
    public boolean isPeaceful() { return peaceful; }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Species other = (Species) o;

        return this.name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder dump = new StringBuilder();

        if (peaceful)
            dump.append("peaceful ");
        else
            dump.append("aggressive ");
        dump.append("species ").append(getName()).append("\n");
        dump.append("- Present states include: \n");
        for (State state : getStates()) {
            dump.append("-- ").append(state).append("\n");
        }

        return dump.toString();
    }

    public static Builder builder() { return new Species().new Builder(); }

    public class Builder {

        private Builder() {}

        public Builder setName(String name) {
            Species.this.name = name;
            return this;
        }
        public Builder setPeaceful(boolean peaceful) {
            Species.this.peaceful = peaceful;
            return this;
        }

        public Species build() { return Species.this; }

    }

}
